## TIME SPENT
This code was developed in the span of 1 hour, 44 minutes (2:49 - 4:33)

## LIBRARIES CONSIDERED & USED
These "fuzzywuzzy" and "levenshtein" libraries are commonly used for word
similarity matches in Python.  Unfortunately, both are licensed under the GPL,
which means that they are de-facto off limits for commercial purposes due to
the license's constraints.

I opted to use SequenceMatcher from the native difflib library. Looking at the
library's code https://github.com/python/cpython/blob/main/Lib/difflib.py I
find that it is very well documented with well-written paragraphs documenting
what the code is doing in great detail as well as clear and concise comments
within the code itself.

## DESIGN CHOICES
I initially wrote this script with functional programming in mind: I wanted
to have clearly-defined functions handle each step I outlined. I planned out
5 steps I needed to accomplish for the code to work:

    1. Get user input;
    2. Read poem file into a dict of line:content;
    3. Calcualte similarity scores between two strings;
    4. Loop over the dict, using function 3 to compare each line to the input;
    5. Return the line(s) with the highest similarity value.

As I began to refine the code, I found more and more opportunities to make the
code simpler and more efficient. For example: Instead of having two steps to
make two dicts (first a lines:content dict, second a lines:similarity created
by reading the first dict.) I opted to consolidate into a single dict by
calculating similarity as each line of content is read and saving the results
as a tuple - line:(content,similarity). Another point of improvement made was
in finding the max similarity value; this was initially written as a long for
loop.  Rewriting this loop using a lambda allowed me to significantly slim down
the code. After making these changes the code had gotten small enough to the
point where containing each detail in its own function would not add much at
all to the code aside from the bulkiness of having too many unnecessary
functions.

## REQUIREMENTS
This code was written in Python version 3.10.6 - because Python is an
interpreted language there is no need to compile the code, just make sure you
have the recent version of Python installed and it should work. If any issue is
encountered, make sure you have a clean install of Python 3.10.6

## OPERATION
Simply enable execution permissions and run ./recall.py and enter your desired
text at the search prompt

## IMPROVEMENTS
1. lepanto.txt is hardcoded in, the code could be rewritten to read a path
specified with a command line arg (ex. ./recall.py -f lepanto.txt)
2. The iteration over each line uses iterating n values for the line numbers.
This is due to concerns I had about duplicate lines causing issues. The n
values feel subjectively clunky to me, I would not be surprised if there were
a more optimal way of writing this code
3. I was not particuarly happy with the performance of the similarity scores
for smaller input strings. I suspect this is primarily due to length, small
but perfectly-matching substrings of a longer line might still have a better
score with another, smaller line. For example, the substring "Sloop" only
appears in line 135, but when used as an input returns lines 95 and 112.
Despite them having completely different text both managed to have the same
(low) similarity score, as well as a common lack of the word "Sloop"
