#!/usr/bin/python3

from difflib import SequenceMatcher as SM

userInput = input("Search poem text:\t")
poem = {}
with open("lepanto.txt",'r') as f:
    n = 1
    for line in f.readlines():
        poem[n] = (line.strip(),SM(None,userInput,line).ratio())
        n += 1
maxSim = max(list(map(lambda x: x[1], poem.values() )))
for x in poem.items():
    if x[1][1] == maxSim:
        print(x[1][0])
